import { createSlice } from '@reduxjs/toolkit'

export const taskSlice = createSlice({
    name: 'tasks',
    initialState: {
        value: [
            {
                id: 1,
                name: 'Buy food!!!',
                tag: '#home',
                isFinished: true,
                deadline: (new Date()).getTime(),
            },
            {
                id: 2,
                name: 'Write Shopping List ',
                tag: '#home',
                isFinished: false,
                deadline: (new Date()).getTime(),
            },
            {
                id: 3,
                name: 'Water flowers!!!',
                tag: '#home',
                isFinished: true,
                deadline: (new Date()).getTime(),
            },
            {
                id: 4,
                name: 'Read a book!!!',
                tag: '#freetime',
                isFinished: true,
                deadline: (new Date()).getTime(),
            }]
    },
    reducers: {
        add: (state, action) => {
            state.value.push(action.payload);
        },
        remove: (state, action) => {
            state.value = state.value.filter((element,index) => index != action.payload)
        },
        change: (state, action) => {
            const index = state.value.map(object => object.id).indexOf(action.payload.id);
            state.value[index] = action.payload;
        }
    },
})

// Action creators are generated for each case reducer function
export const { add, change, remove } = taskSlice.actions

export default taskSlice.reducer