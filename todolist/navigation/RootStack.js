import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from '../views/component/HomeScreen';
import AddTaskScreen from '../views/component/AddTaskScreen';
import DetailTaskScreen from '../views/component/DetailTaskScreen';


const Stack = createNativeStackNavigator();

export default function RootStack() {
  return (
    <Stack.Navigator initialRouteName="Home">
    <Stack.Screen name="Home" component={HomeScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen name="AddTask" component={AddTaskScreen}
      options={{ headerTransparent: true, title: '', headerTintColor: '#fff' }}
    />
    <Stack.Screen name="DetailTask" component={DetailTaskScreen}
      options={{ headerTransparent: true, title: '', headerTintColor: '#fff' }}
    />
  </Stack.Navigator>
  )
}