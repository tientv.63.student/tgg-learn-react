import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { Button, Dimensions, FlatList, Modal, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/AntDesign';
import { useState } from 'react';
import { AddTagModal } from './components/AddTagModal'
import DateTimePicker from '@react-native-community/datetimepicker';
import { Provider } from 'react-redux'
import store from './redux/store'
import RootStack from './navigation/RootStack';

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootStack/>
      </NavigationContainer>
    </Provider>
  );
};
