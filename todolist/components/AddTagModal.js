import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

const AddTagModal = (props) => {
    const [tagName, settagName] = useState('');
    const closeModal = (bool,data) => {
        props.changeModalVisible(bool);
        props.setData(data);
    }
    return (
        <TouchableOpacity style={styles.container} disabled={true} >
            <View style={styles.modal} >
                <Text style={styles.textHeading}> Add tags</Text>
                <TextInput
                    placeholder="Add tags"
                    placeholderTextColor={'#C5C5C5'}
                    fontSize={13}
                    onChangeText={newText => settagName(newText)}
                />
                <TouchableOpacity 
                onPress={() =>{closeModal(false,tagName)}}
                style={styles.touchableOpacity}
                ><Text style={styles.textYellow}>Add</Text></TouchableOpacity>
            </View>
        </TouchableOpacity>
    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#333'
    },
    modal: {
        height: 135,
        width: 335,
        paddingTop: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10
    },
    textHeading: {
        fontSize: 20,
        lineHeight: 24,
        fontWeight: 'bold'
    },
    touchableOpacity: {
        paddingVertical: 10,
        paddingVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',

    },
    textYellow: {
        color: '#F6CF00',
        fontWeight: '700'
    }
})
export { AddTagModal } 