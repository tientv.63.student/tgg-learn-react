import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import styles from '../style/styles';
import textStyle from '../style/textStyle';
import boxStyle from '../style/boxStyle';
import backgroundColor from '../style/backgroundColor';
import Icon from 'react-native-vector-icons/AntDesign';
import store from '../../redux/store'

function HomeScreen({ navigation, route }) {
    const tasks = useSelector(state => state.tasks.value)
    const dispatch = useDispatch();
    const goToDetails = (id) => {
      navigation.navigate('DetailTask', {
        idDetails: id,
      })
    }
    const renderItem = ({ item, index }) => (
      <View style={styles.task}>
      {console.log(index)}
        <View style={styles.checkRadio}>
          {item.isFinished ? <Icon name="check" size={30} color="#000" /> : null}
        </View>
        <TouchableOpacity
          onPress={() => goToDetails(index)}
        >
          <View style={styles.taskInfor}>
            <Text style={[styles.taskName]}>{item.name}</Text>
            <Text style={[styles.taskTag]}>{item.tag}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  
    return (
      <View style={styles.container}>
        <View style={styles.heading}>
          <Text style={[styles.textHeading, styles.dateHeading]}>20 January 2020</Text>
          <Text style={[styles.textHeading, styles.taskNumber]}>{tasks.length} Task</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.todoList}>
            <FlatList
              data={tasks}
              renderItem={renderItem}
              keyExtractor={(item,index) => index}
            />
          </View>
        </View>
        <View style={styles.addTaskWrap}>
          <TouchableOpacity style={styles.addBtn}
            onPress={() => navigation.navigate('AddTask')}
          >
            <Icon
              name="plus"
              size={30}
              color="white"
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  
export default HomeScreen