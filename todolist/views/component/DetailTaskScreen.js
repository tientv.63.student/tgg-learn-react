import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import styles from '../style/styles';
import textStyle from '../style/textStyle';
import boxStyle from '../style/boxStyle';
import backgroundColor from '../style/backgroundColor';
import Icon from 'react-native-vector-icons/AntDesign';
import store from '../../redux/store'
import  { add, change, remove }  from '../../redux/slice'

function DetailTaskScreen({ navigation, route }) {
    const tasks = useSelector(state => state.tasks.value)
    const dispatch = useDispatch();
    const taskSwap = tasks[route.params.idDetails];
    const [task, setTask] = useState(taskSwap);
    const removeTask = () => {
      dispatch(remove([route.params.idDetails]));
      navigation.goBack();
    }
    const checkTask = () => {
      dispatch(change({...task, isFinished:true}));
      navigation.goBack();
    }
    return (
      <View style={[styles.bgGray, styles.addTaskContainer]}>
        <View style={[styles.formInput]}>
          <View style={styles.lineInputForm}>
            <Text style={textStyle.textYellow}>
              To-do
            </Text>
            <TextInput
              style={[boxStyle.marginTop8, styles.input, textStyle.textWhite, textStyle.textSize24]}
              placeholder="Task name"
              placeholderTextColor="#fff"
              value={task.name}
              onChangeText={newText => setTaskName(newText)}
            />
          </View>
          <View style={[styles.lineInputForm, boxStyle.marginTop32]}>
            <Text style={textStyle.textYellow}>
              Tags
            </Text>
            <Text style={[boxStyle.marginLeft8, textStyle.textWhite]}>{task.tag}</Text>
            <TouchableOpacity style={[boxStyle.row, boxStyle.rowCenter]}
              onPress={() => { setIsAddTagModalVisible(true) }}
            >
              <View style={[boxStyle.marginTop8, backgroundColor.white, boxStyle.rounded50, styles.btnAddPlus]}>
                <Icon name="plus" size={15} color="#000" />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.lineInputForm, boxStyle.marginTop32]}>
            <Text style={textStyle.textYellow}>
              Deadline
            </Text>
            <TouchableOpacity style={[boxStyle.row, boxStyle.rowCenter]}
            >
              <View style={[boxStyle.marginTop8]}>
                <Icon name="calendar" size={30} color="#fff" />
              </View>
              <Text style={[boxStyle.marginLeft8, textStyle.textWhite, textStyle.textSize24]}>{(new Date(task.deadline)).toLocaleString()}</Text>
            </TouchableOpacity>
          </View>
  
        </View>
        <View style={[styles.footer, boxStyle.row]}>
          <TouchableOpacity style={[boxStyle.rounded50, backgroundColor.red, { width: 50, height: 50 }, boxStyle.colCenter, boxStyle.rowCenter]}
            onPress={() => removeTask()}
          >
            <Icon
              name="close"
              size={30}
              color="#fff"
            />
          </TouchableOpacity>
          <TouchableOpacity style={[boxStyle.rounded50, backgroundColor.white, { width: 50, height: 50 }, boxStyle.colCenter, boxStyle.rowCenter]}
            onPress={() => checkTask()}
          >
            <Icon
              name="check"
              size={30}
              color="#000"
            />
          </TouchableOpacity>
        </View>
  
      </View>
    )
  }

export default DetailTaskScreen
