import { Modal, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import styles from '../style/styles';
import textStyle from '../style/textStyle';
import boxStyle from '../style/boxStyle';
import backgroundColor from '../style/backgroundColor';
import Icon from 'react-native-vector-icons/AntDesign';
import { useDispatch, useSelector } from 'react-redux';
import { AddTagModal } from '../../components/AddTagModal'
import store from '../../redux/store'
import  { add, change, remove }  from '../../redux/slice'
function AddTaskScreen({ navigation }) {
    const [isAddTagModalVisible, setIsAddTagModalVisible] = useState(false);
    const [tagName, settagName] = useState('');
    const [taskName, setTaskName] = useState('');
    const [isShowDatePicker, setIsShowDatePicker] = useState(false);
    const changeModalVisible = (bool) => {
        setIsAddTagModalVisible(bool);
    }
    const setData = (data) => {
        if (data != '') {
            settagName('#' + data);
        }
    }
    const [date, setDate] = useState(new Date());
    const [fdate, setFDate] = useState('Today');
    const tasks = useSelector(state => state.tasks.value)
    const dispatch = useDispatch();

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate;
        setIsShowDatePicker(false);
        setDate(currentDate);
        setFDate(date.toLocaleString())
    };
    const saveTask = () => {
        if (tagName !== '' && taskName !== '') {

            dispatch(add({
                id: tasks.length
                , name: taskName,
                tag: tagName,
                isFinished: false,
                deadline: date.getTime(),
            }));
            navigation.goBack();
        }
    }

    return (
        <View style={[styles.bgGray, styles.addTaskContainer]}>
            <View style={[styles.formInput]}>
                <View style={styles.lineInputForm}>
                    <Text style={textStyle.textYellow}>
                        To-do
                    </Text>
                    <TextInput
                        style={[boxStyle.marginTop8, styles.input, textStyle.textWhite, textStyle.textSize24]}
                        placeholder="Task name"
                        placeholderTextColor="#fff"
                        onChangeText={newText => setTaskName(newText)}
                    />
                </View>
                <View style={[styles.lineInputForm, boxStyle.marginTop32]}>
                    <Text style={textStyle.textYellow}>
                        Tags
                    </Text>
                    <Text style={[boxStyle.marginLeft8, textStyle.textWhite]}>{tagName}</Text>
                    <TouchableOpacity style={[boxStyle.row, boxStyle.rowCenter]}
                        onPress={() => { setIsAddTagModalVisible(true) }}
                    >
                        <View style={[boxStyle.marginTop8, backgroundColor.white, boxStyle.rounded50, styles.btnAddPlus]}>
                            <Icon name="plus" size={15} color="#000" />
                        </View>
                        <Text style={[boxStyle.marginLeft8, textStyle.textWhite]}>Add Tags</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.lineInputForm, boxStyle.marginTop32]}>
                    <Text style={textStyle.textYellow}>
                        Deadline
                    </Text>
                    <TouchableOpacity style={[boxStyle.row, boxStyle.rowCenter]}
                        onPress={() => { setIsShowDatePicker(true) }}
                    >
                        <View style={[boxStyle.marginTop8]}>
                            <Icon name="calendar" size={30} color="#fff" />
                        </View>
                        <Text style={[boxStyle.marginLeft8, textStyle.textWhite, textStyle.textSize24]}>{fdate}</Text>
                    </TouchableOpacity>
                </View>

            </View>
            <View style={[styles.boxWrapSaveBtn]}>
                <TouchableOpacity style={styles.saveBtn} onPress={() => { saveTask() }}>
                    <Text style={textStyle.textWhite}>Save</Text>
                </TouchableOpacity>
            </View>
            <Modal
                transparent={true}
                animationType="fade"
                visible={isAddTagModalVisible}
                nRequestClose={() => changeModalVisible(false)}
            >
                <AddTagModal
                    changeModalVisible={changeModalVisible}
                    setData={setData}
                />
            </Modal>
            {isShowDatePicker &&
                <DateTimePicker
                    mode='date'
                    display='default'
                    onChange={onChange}
                    value={date}
                >

                </DateTimePicker>
            }
        </View>
    )
}

export default AddTaskScreen
