import { StyleSheet } from 'react-native';

const boxStyle = StyleSheet.create({
    row: {
      flexDirection: 'row'
    },
    rowCenter: {
      alignItems: 'center',
    },
    colCenter: {
      justifyContent: 'center',
    }
    ,
    rounded50: {
      borderRadius: 50
    },
    rounded100: {
      borderRadius: 100
    }
    ,
    marginTop8: {
      marginTop: 8
    },
    marginLeft8: {
      marginLeft: 8
    },
    marginTop32: {
      marginTop: 32
    }
  });
export default boxStyle