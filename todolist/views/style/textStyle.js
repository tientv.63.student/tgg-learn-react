import { StyleSheet } from 'react-native';

const textStyle = StyleSheet.create({
    textYellow: {
        color: '#FFF500'
    },

    textSize16: {
        fontSize: 16
    },
    textSize24: {
        fontSize: 24
    }
    ,
    textWhite: {
        color: 'white'
    },
})

export default textStyle;