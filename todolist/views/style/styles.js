import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFF500',
      paddingTop: 50,
      paddingHorizontal: 20
    },
    heading: {
      flex: 1,
    },
    textHeading: {
    },
    dateHeading: {
      fontSize: 32,
      lineHeight: 38,
      fontWeight: '700'
    },
    taskNumber: {
      fontSize: 24,
      lineHeight: 29,
      marginTop: 8
    }
    ,
    body: {
      flex: 5,
    },
    task: {
      flexDirection: 'row',
    },
    taskInfor: {
      marginLeft: 16
    }
    ,
    taskName: {
      fontSize: 24,
    }
    ,
    checkRadio: {
      borderWidth: 1,
      width: 32,
      height: 32,
      borderRadius: 50,
      marginTop: 10
    },
    addTaskWrap: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
    addBtn: {
      borderRadius: 100,
      width: 56,
      height: 56,
      backgroundColor: '#303030',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 24
    },
    bgGray: {
      backgroundColor: '#303030'
    },
    addTaskContainer: {
      flex: 1,
      paddingTop: 150,
      paddingHorizontal: 24
    },
    saveBtn: {
      backgroundColor: '#F6CF00'
      , padding: 15,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 4,
      marginBottom: 0
    },
    boxWrapSaveBtn: {
      flex: 1,
    },
    formInput: {
      flex: 7
    },
    btnAddPlus: {
      width: 24,
      height: 24,
      justifyContent: 'center',
      alignItems: 'center',
    }
    , footer: {
      flex: 1,
      justifyContent: 'space-between'
    }
  });
export default styles;